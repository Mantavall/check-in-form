
// Date Picker
$(function () {
  $('#checkIn, #checkOut').datepicker({
      format: "dd/mm/yyyy",
      language: "es",
      autoclose: true,
      todayHighlight: true,
      startDate: '0d',
      endDate:'19/06/2020' ,
      daysOfWeekDisabled: [0,6],
      weekStart: '1'
  });
  // automatically open Check Out form , when Check In is selected
  $('#checkIn').datepicker().on('changeDate', function(e) {
    $('#checkOut').datepicker().datepicker( "show");
  });

  // prevent from selecting earlier day
  $('#checkIn').datepicker({autoClose: true}).on('changeDate',function(e) {
    $('#checkOut').datepicker({autoClose: true}).datepicker('setStartDate', e.date);
  })
  
  //filter weekdays
  $('.stayType').click(function() {
    if ($(this).is(':checked')) {
      $('#checkOut').datepicker('setDaysOfWeekDisabled',[1,2,3,4]);
      $('#checkIn').datepicker('setDaysOfWeekDisabled', [1,2,3,4]);
    }
  });

  //filter weekends
  $(".stayType").click(function() {  
    var ischecked= $(this).is(':checked');
    if(!ischecked) {
      $('#checkOut').datepicker('setDaysOfWeekDisabled',[0,6]);
      $('#checkIn').datepicker('setDaysOfWeekDisabled', [0,6]);
    }  
  }); 
});

//When Submit button is clicked get data from the datepicker.
function getData() {
  var checkInField = $("#checkIn").data('datepicker').getFormattedDate('yyyy-mm-dd');
  var checkOutField = $("#checkOut").data('datepicker').getFormattedDate('yyyy-mm-dd');
  //checking whether input is not missing
  if( checkInField === "" || checkOutField === "") {
    $('.form-control').addClass("is-invalid");
    $('p').removeClass("d-none");
    return 
  } 

  $('p').addClass("d-none");
  $('.form-control').removeClass("is-invalid");

  //display selected dates on the page
  document.getElementById("selectedInDate").innerHTML = checkInField;
  document.getElementById("selectedOutDate").innerHTML = checkOutField;
};

